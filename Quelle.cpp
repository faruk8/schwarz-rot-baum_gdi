#include <iostream>
#include <ostream>
#include <time.h>
#include "SearchTree.h"
#include <queue>
#include <deque>
#include <fstream>
#include <string>



#define ARRAY_SIZE 2000
using namespace std;
void createTreeL(SearchTree<int>& tree)
{
    tree.insert(16);
    tree.insert(24);
    tree.insert(8);
    tree.insert(28);
    tree.insert(20);
    tree.insert(12);
    tree.insert(4);


}

void createTree(SearchTree<int>& tree)
{
    tree.insert(16);
    tree.insert(24);
    tree.insert(8);
    tree.insert(28);
    tree.insert(20);
    tree.insert(12);
    tree.insert(4);
    tree.insert(30);
    tree.insert(26);
    tree.insert(22);
    tree.insert(18);
    tree.insert(14);
    tree.insert(10);
    tree.insert(6);
    tree.insert(2);
    tree.insert(31);
    tree.insert(29);
    tree.insert(27);
    tree.insert(25);
    tree.insert(23);
    tree.insert(21);
    tree.insert(19);
    tree.insert(17);
    tree.insert(15);
    tree.insert(13);
    tree.insert(11);
    tree.insert(9);
    tree.insert(7);
    tree.insert(5);
    tree.insert(3);
    tree.insert(1);


}
/*
* Test fuer Right Rotation
*/
void testRightRotate1()
{
    SearchTree<int> tree;
    createTree(tree);
    tree.rigthrotate(tree.search(16));
    cout << "Erster Test: Rotate nach rechts an Knoten 16" << endl;
    tree.print();

}
void testRightRotate2()
{
    SearchTree<int> tree;
    createTree(tree);
    tree.rigthrotate(tree.search(24));
    cout << "Zweiter Test: Rotate nach rechts an Knoten 24" << endl;
    tree.print();
}
void testRightRotate3()
{
    SearchTree<int> tree;
    createTree(tree);
    tree.rigthrotate(tree.search(10));
    cout << "Dritter Test: Rotate nach rechts an Knoten 10" << endl;
    tree.print();
}
/*
* Test fuer Left Rotation
*/
void testLeftRotate1()
{
    SearchTree<int> tree;
    createTreeL(tree);
    tree.leftrotate(tree.search(16));
    cout << "Erster Test: Rotate nach links an Knoten 16" << endl;
    tree.print();

}
void testLeftRotate2()
{
    SearchTree<int> tree;
    createTreeL(tree);
    tree.leftrotate(tree.search(24));
    cout << "Zweiter Test: Rotate nach links an Knoten 24" << endl;
    tree.print();
}
void testLeftRotate3()
{
    SearchTree<int> tree;
    createTreeL(tree);
    tree.leftrotate(tree.search(8));
    cout << "Dritter Test: Rotate nach links an Knoten 10" << endl;
    tree.print();
}

using namespace std;

int main(int argc, char* argv[])
{
    srand(time(0));

    SearchTree<int> tree;
   


//Test f�r RightRotate (die Right und left Testf�lle programmiert von Faruk Ertem) 
    //testRightRotate1();
    //testRightRotate2();
    //testRightRotate3();
   
 
   // testLeftRotate1();
   // testLeftRotate2();
    //testLeftRotate3();

    /*
    * Laufzetimessung mit echt Daten (Bearbeitet von Faruk Ertem)  
    */

    /*double langengrad = 0.0;

    char datum[25];
    char readbuffer[ARRAY_SIZE];



    char* filename;
    char defaultfile[] = "33RO20010714_CO2_underway_SOCATv3.tab";


    FILE* input_dat;
   

  
   if (argc == 2)
    {

        filename = argv[1];
    }
    else
    {
        filename = defaultfile;
    }
   input_dat = fopen(filename, "r");                                     //datei �ffnen

   float zeit;                                                          //variablen f�r die Zeitmessung
   clock_t start, ende;
   zeit = 0;
   int gesamtZeit = 0;

    if (input_dat == nullptr)
        printf("Datei oeffnen fehlgeschlagen\n");                               //�berpr�fung, ob datei ge�ffnet werden konnte
    else
    {
        while (!feof(input_dat))                                              //laufen durch die Datei bis man am ende ist mit feof
        {

            char* x = fgets(readbuffer, sizeof(readbuffer) - 1, input_dat);       // in read buffer speichern

            if (x != nullptr)
            {
                if (sscanf(readbuffer, "%s %lf",datum,&langengrad) == 2)
                {
                    start = clock();
                    tree.insert(langengrad);
                    ende = clock();

                    gesamtZeit = gesamtZeit + (ende - start);
                    
                    
                }
                else
                    printf("Fehler:%s\n", readbuffer);

                
               
            }
            
        }
        zeit = (float)(gesamtZeit) / (float)CLOCKS_PER_SEC;
        
        printf("\nDIE LAUFZEITMESSUNG :%.2f ", zeit);
    }*/


   /*
   *Laufzeitmessung von Schwarz- Rot B�ume mit zufaelligen Zahlen. (Programmiert von Faruk Ertem)
   *
   */
    /*float zeit;
    clock_t start, ende;

    zeit = 0;
    int insertMax = 51000000;

    
    int z = 0;
    int gesamtZeit = 0
    for (int i = 1; i < insertMax; i++)
    {

        z = ((rand() + rand() * (RAND_MAX - 1.0)) / RAND_MAX) * 1000000 / RAND_MAX;

        start = clock();
        tree.insert(z);
        ende = clock();

        gesamtZeit = gesamtZeit + (ende - start);
    }

    zeit = (float)(gesamtZeit) / (float)CLOCKS_PER_SEC;
    printf("\nDIE LAUFZEITMESSUNG :%.2f ", zeit);*/
    
    /*
    *warst case beispiel (programmiert Gizem Aslan)
    */
    float zeit;
    clock_t start, ende;

    zeit = 0;
    int baum_einfuegen = 5000000;
    int durchlaufe = 10;

   
    int gesamtZeit = 0;
    for (int k = 0; k < durchlaufe; k++)
    {
        for (int i = 1; i < baum_einfuegen; i++)
        {

            start = clock();
            tree.insert(i);
            ende = clock();

            gesamtZeit = gesamtZeit + (ende - start);
        }
       zeit = (float)(gesamtZeit) / (float)CLOCKS_PER_SEC;
       printf("\nDIE LAUFZEITMESSUNG :%.2f ", zeit);
       baum_einfuegen += 5000000;
    }
    

    
        
    

    
   

    return 0;
}
