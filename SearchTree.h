#include <iostream>
using namespace std;

template<typename T>
class SearchTree;




template<typename T>
class TreeNode
{
    friend class SearchTree<T>;

public:
    TreeNode* parent = nullptr;
    TreeNode* left = nullptr;
    TreeNode* right = nullptr;

public:
    const T key;
     T colour;
     
public:
    TreeNode(const T key = -1) : key(key) {}
    /*virtual ~TreeNode()
    {
        if (left != this)
        {
            delete left;
        }
            
        if (right != this)
        {
            delete right;
        }
            

    }*/

    // Disallow (accidental) copying or moving:
   TreeNode(const TreeNode& copyFrom) = delete;
    TreeNode(TreeNode&& moveFrom) = delete;

public:

   TreeNode* predecessor()
    {
        if (this->left != nullptr)
            return this->left->maximum();
    }

    TreeNode* successor()
    {
        if (this->right != nullptr)
            return this->right->minimum();
    }

    TreeNode* minimum()
    {
        TreeNode* x = this;
        while (x->left != nullptr)
            x = x->left;
        return x;
    }
    TreeNode* maximum()
    {
        TreeNode* x = this;
        while (x->right != nullptr)
            x = x - right;
        return x;
    }

    /*
    *search ist eine Methode um ein Schuessel zu suchen
    *Sie wird Rekusiv aufgerufen, bis "knoten key " == "gesuchter key" ist(programmiert von Faruk Ertem)
    */
    TreeNode* search(T key)
    {
        if (this->key == key)             //wenn das der Schluessel ist return wir ihn
            return this;
        if (this->key > key)               //wenn der schl�ssel kleiner ist als der knoten schluessel
        {
            if (left == nullptr)     // wenn linker pointer null ist dann den linken ausgeben
                return left;
            return left->search(key);//falls nicht search rekusiv
        }
        else
        {
            if (right == nullptr)        // ansonsten den rechten zweig ausgeben
                return right;
            return right->search(key);
        }
    }

public:
    // optional, aber praktisch zum debuggen:
    friend std::ostream& operator<<(std::ostream& cout, const TreeNode* tree)
    {
        if (tree == nullptr) return cout; // nothing to print

        cout << tree->left << tree->key << ", " << tree->right;

        return cout;
    }
};


template<typename T>
class SearchTree
{
    using Node = TreeNode<T>; // optional, Fuer uebersichtlichen Code



private:
    Node* root; // Wurzel (im Falle eines leeren Baumes: nullptr)
    Node* waechter; //waechter ist immer vorhanden 

public:
    SearchTree(): waechter(new Node) { root = waechter; }
    virtual ~SearchTree()
    {
        delete root;
    }

    // Disallow (accidental) copying or moving:
    SearchTree(const SearchTree& copyFrom) = delete;
    SearchTree(SearchTree&& moveFrom) = delete;

private:
    void transplant(const Node* const nodeToReplace, Node* const replacementNode)
    {
        if (nodeToReplace->parent == nullptr)
        {
            root = replacementNode;
        }
        else if (nodeToReplace == nodeToReplace->parent->left)
        {
            nodeToReplace->parent->left = replacementNode;
        }
        else
        {
            nodeToReplace->parent->right = replacementNode;
        }
        if (replacementNode != nullptr)
            replacementNode->parent = nodeToReplace->parent;
    }

public:
    /*
    * Um verloren gegangene Eigenschaften wiederherzustellen. 
    * M�ssen wir die Farben und die Zeigerstruktur des Baumes �ndern. Das machen unsere Rotationen (programmiert von Gizem Aslan)
    */
    void leftrotate(Node* x)
    {
        Node* y = x->right;     // x retch wird unser y
        x->right = y->left;     // unser y left wird unser x right

        if (y->left != waechter)   
        {
            y->left->parent = x;   
        }
        y->parent = x->parent; // x vater �st jetzt y vater

        if (x->parent == waechter)  // wenn unser vater == waechter ist, ist es die Wurzel 
        {
            root = y;           
        }
        else if (x == x->parent->left)// wenn unser key  == key vater left ist 
        {
            x->parent->left = y;// wird y  = key vater links geschrieben
        }
        else
        {
            x->parent->right = y;// wird y  = key vater rechts geschrieben 
        }
        y->left = x; // key wird y links
        x->parent = y;// y wird vater von x
    }
    /*
    * Um verloren gegangene Eigenschaften wiederherzustellen.
    * M�ssen wir die Farben und die Zeigerstruktur des Baumes �ndern. Das machen unsere Rotationen (programmiert von Gizem Aslan)
    */
    void rigthrotate(Node* key)
    {
        Node* y = key->left; // x retch wird unser y

        key->left = y->right; // unser y left wird unser x right

        if (y->right != waechter)
        {
            y->right->parent = key;
        }
           
        y->parent = key->parent; // x vater �st jetzt y vater

        if (key->parent == waechter)// wenn unser vater == waechter ist, ist es die Wurzel 
        {
            root = y;
        }
           
        else if (key == key->parent->right) // wenn unser key  == key vater recht ist 
        {
            key->parent->right = y;  // wird y  = key vater rechts geschrieben 
        }
            
        else
        {
            key->parent->left = y;// wird y  = key vater links geschrieben
        }

        y->right = key;  // key wird y rechts
        key->parent = y; // y wird vater von x

    }
public:
    /*
    *Insert ist eine Methode zum einf�gen der neuen Werte ins Binaer Baum.(programmiert von Faruk Ertem)
    */
    void insert(const T key)
    {
        Node* y = waechter;   //wird auf den waechter gesetzt (weil in schwarz rot b�umen nichts auf nullptr zeigt)
        Node* newNode = new Node(key);  // newNode ist unser neuer Knoten
        Node* x = root;                  //x ist unser Wurzel
    
        while (x != waechter)               //solange unser knoten nicht auf waechter zeigt 
        {
            y = x;                   //soll unser vorg�ngerzeiger gleich unser zeigerknoten sein

            if (newNode->key < x->key) //wenn unser schl�ssel den wir einf�gen wollen kleiner ist als der schl�ssel vom knoten
                x = x->left;            //dann geht unser x "ein nach links"
            else                        
                x = x->right;            //ansonsten geht unser x ein nach rechts
        }

        //Knoten erstellen
        newNode->parent = y;

        if (y == waechter)
        {
            root = newNode;
        }
        else if (newNode->key < y->key)
        {
            y->left = newNode;     //f�gt neues knoten links ein
        }
        else
        {
            y->right = newNode;    //f�gt neues knoten rechts ein
        }
        newNode->left = waechter;
        newNode->right = waechter;
        newNode->colour = 1;

        insertfix(newNode);

    }
    /**
    * Die Methode insertfix benoetigt man um die Eigenschaften der Rot-Schwarz Baeume immer zu garantieren.(programmiert von Faruk Ertem)
    * 
    */
    void insertfix(Node* z)
    {
        while (z->parent->colour == 1)  // ueberpruefen, ob der Vater rot ist 
        {
            if (z->parent == z->parent->parent->left) // wenn key vater == key opa left dann geht man in die if anweisung
            {
                Node* y = z->parent->parent->right;  // y wird unser onkel rechts
            
                if (y->colour == 1)                // wenn unser onkel rot ist dann 
                {
                    z->parent->colour = 0;        //setzen wir key vater schwarz  // FALL1
                    y->colour = 0;                // und onkel auf schwarz        // FALL1
                    z->parent->parent->colour = 1; // opa wird rot                // FALL1
                    z = z->parent->parent;        // neuer z ist unser opa        // FALL1
                }
                else 
                {
                    if (z == z->parent->right)  //wenn unser key == key vater rechts ist gehen wir in die if anweisung
                    {
                        z = z->parent;          // vater wird der neue z  //FALL 2
                        leftrotate(z);          //                        //FALL 2
                    }
                    z->parent->colour = 0;     // vater wird schwarz      //FALL3 
                    z->parent->parent->colour = 1;// opa rot              //FALL3
                    rigthrotate(z->parent->parent);//                     //FALL3
                }   
            }
            else
            {
                if (z->parent == z->parent->parent->right) // wenn key vater == key opa right dann geht man in die if anweisung
                {
                    Node* y = z->parent->parent->left;  // y wird unser onkel links
                    if (y->colour == 1)                 // wenn unser onkel rot ist dann
                    {
                        z->parent->colour = 0;          //setzen wir key vater schwarz  // FALL1
                        y->colour = 0;                  // und onkel auf schwarz        // FALL1
                        z->parent->parent->colour = 1;  // opa wird rot                // FALL1
                        z = z->parent->parent;          // neuer z ist unser opa        // FALL1
                    }
                    else 
                    {
                        if (z == z->parent->left)   //wenn unser key == key vater links ist gehen wir in die if anweisung
                        {
                            z = z->parent;              // vater wird der neue z  //FALL 2
                            rigthrotate(z);             //                        //FALL 2
                        }
                        z->parent->colour = 0;          // vater wird schwarz     //FALL3 
                        z->parent->parent->colour = 1;  // opa rot                //FALL3
                        leftrotate(z->parent->parent);  //                        //FALL3
                    } 
                }
            }         
        }
        root->colour = 0;// wurzel am ende immer schwarz machen
    }
     /*
     *L�scht ein Knoten (programmiert von Gizem Aslan)
     */
    void deleteNode(Node* const node) 
    {
        if (node->left == nullptr)
        {
            transplant(node, node->right);
        }
        else if (node->right == nullptr)
        {
            transplant(node, node->left);
        }
        else
        {
            Node* y = node->right->minimum();

            if (y->parent != node)
            {
                transplant(y, y->right);
                y->right = node->right;
                y->right->parent = y;
            }
            transplant(node, y);
            y->left = node->left;
            y->left->parent = y;
        }
    }


    Node* search(const T key)
    {
        if (root == nullptr)
            return nullptr;

        return root->search(key);
    }

    Node* minimum()
    {
        return root->minimum();
    }
    Node* maximum()
    {
        return root->maximum();
    }


public:
    // optional, aber praktisch zum debuggen:
    friend std::ostream& operator<<(std::ostream& cout, const SearchTree& tree)
    {
        // cout << tree.root; // markiert rootNode nicht
        cout << tree.root->left << "<" << tree.root->key << ">, " << tree.root->right; // markiert rootNode
        return cout;
    }


    void printSubtree(const Node* tree,  const size_t depth)
    {
        if (tree == waechter) return;

        printSubtree(tree->right, depth + 1);

        for (size_t i = 0; i < depth; i++)
            std::cout << "\t";
        std::cout << tree->key <<"("<<tree->colour<<")" << "\n";

        printSubtree(tree->left, depth + 1);
    }

    void print()
    {
        printSubtree(root, 0);
    }
};
